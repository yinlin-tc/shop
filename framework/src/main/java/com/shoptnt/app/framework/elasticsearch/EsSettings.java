/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.framework.elasticsearch;

/**
 *
 * es索引设置
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-03-21
 */

public class EsSettings {

    /**
     * 商品索引后缀
     */
    public static final String GOODS_INDEX_NAME="goods";

    /**
     * 商品type名字
     */
    public static final String GOODS_TYPE_NAME="goods";

    /**
     * 拼团索引后缀
     */
    public static final String PINTUAN_INDEX_NAME="pt";


    /**
     * 拼团类型名字
     */
    public static final String PINTUAN_TYPE_NAME="pintuan_goods";


}
