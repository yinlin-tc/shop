/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.framework.security.model;

/**
 * 角色字义
 * Created by kingapex on 2018/3/11.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/3/11
 */
public enum Role {

    /**
     * 买家角色
     */
    BUYER,

    /**
     * 卖家角色
     */
    SELLER,
    /**
     * 店员角色
     */
    CLERK


}
