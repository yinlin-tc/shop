/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.trade.order.model.enums;

/**
 * Created by kingapex on 2019-02-11.
 * 订单个性化数据key
 *
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-02-11
 */
public enum OrderDataKey {

    /**
     * 拼团
     */
    pintuan,

    /**
     * 测试用
     */
    test
}
