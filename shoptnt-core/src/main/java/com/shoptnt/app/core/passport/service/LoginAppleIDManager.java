/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.passport.service;



import com.shoptnt.app.core.member.model.dto.AppleIDUserDTO;

import java.util.Map;

/**
 * AppleID IOS 登陆服务
 * @author snow
 * @since v1.0
 * @version 7.2.2
 * 2020-12-16
 */
public interface LoginAppleIDManager {

    /**
     * IOS-APP 登录
     * @param uuid
     * @param appleIDUserDTO
     * @return
     */
    Map appleIDLogin(String uuid, AppleIDUserDTO appleIDUserDTO);



}
