/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.trade.order.model.enums;

/**
 * Created by kingapex on 2019-01-27.
 * 订单类型
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-01-27
 */
public enum OrderTypeEnum {

    /**
     * 普通订单
     */
    normal,

    /**
     * 拼团订单
     */
    pintuan

}
