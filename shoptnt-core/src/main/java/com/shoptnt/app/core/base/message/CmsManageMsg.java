/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.base.message;

import java.io.Serializable;

/**
 * 为发送消息提供的模型 
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午9:47:38
 */
public class CmsManageMsg implements Serializable{
	
}
