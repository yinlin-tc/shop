/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.client.system.impl;

import com.shoptnt.app.core.base.SceneType;
import com.shoptnt.app.core.base.model.vo.SmsSendVO;
import com.shoptnt.app.core.base.service.SmsManager;
import com.shoptnt.app.core.client.system.SmsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 短信实现
 *
 * @author zh
 * @version v7.0
 * @date 18/7/31 上午11:13
 * @since v7.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class SmsClientDefaultImpl implements SmsClient {

    @Autowired
    private SmsManager smsManager;


    @Override
    public boolean valid(String scene, String mobile, String code) {
        return smsManager.valid(scene, mobile, code);
    }

    @Override
    public void sendSmsMessage(String byName, String mobile, SceneType sceneType) {
        this.smsManager.sendSmsMessage(byName, mobile, sceneType);
    }

    @Override
    public void send(SmsSendVO smsSendVO) {
        smsManager.send(smsSendVO);
    }
}
