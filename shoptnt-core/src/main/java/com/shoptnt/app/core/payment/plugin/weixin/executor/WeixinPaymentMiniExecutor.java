/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.payment.plugin.weixin.executor;

import com.shoptnt.app.core.payment.model.vo.PayBill;
import com.shoptnt.app.core.payment.plugin.weixin.WeixinPuginConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author fk
 * @version v2.0
 * @Description: 微信wap端
 * @date 2018/4/1810:12
 * @since v7.0.0
 */
@Service
public class WeixinPaymentMiniExecutor extends WeixinPuginConfig {

    @Autowired
    private WeixinPaymentJsapiExecutor weixinPaymentJsapiExecutor;

    /**
     * 支付
     *
     * @param bill
     * @return
     */
    public Map onPay(PayBill bill) {
        return weixinPaymentJsapiExecutor.onPay(bill);
    }


}
